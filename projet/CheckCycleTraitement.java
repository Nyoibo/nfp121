import java.util.HashSet;
import java.util.Set;
import java.util.Collections;
public class CheckCycleTraitement extends Traitement {
    public void ajouterTraitementsAvecVerification(Traitement... nouveauxTraitements) {
        for (Traitement nouveau : nouveauxTraitements) {
            if (detecterCycle(nouveau, new HashSet<>())) {
                throw new IllegalStateException("Ajout de ce traitement créerait un cycle.");
            }
        }
        Collections.addAll(suivants, nouveauxTraitements);
    }

    private boolean detecterCycle(Traitement traitement, Set<Traitement> dejaVus) {
        if (dejaVus.contains(traitement)) {
            return true;
        }
        dejaVus.add(traitement);
        for (Traitement t : traitement.suivants) {
            if (detecterCycle(t, new HashSet<>(dejaVus))) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void traiter(Position position, double valeur) {
        for (Traitement suivant : suivants) {
            suivant.traiter(position, valeur);
        }
    }
}
