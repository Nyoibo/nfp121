import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.FileWriter;
import java.io.IOException;

public class SaisiesSwing extends JFrame {
    private JTextField txtAbscisse, txtOrdonnee, txtValeur;
    private JButton btnValider, btnEffacer, btnTerminer;

    public SaisiesSwing() {
        setTitle("Saisie données");
        setSize(400, 200);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();

        txtAbscisse = new JTextField(10);
        txtOrdonnee = new JTextField(10);
        txtValeur = new JTextField(10);

        btnValider = new JButton("Valider");
        btnEffacer = new JButton("Effacer");
        btnTerminer = new JButton("Terminer");

        setupGridBag(gbc);

        setVisible(true);
    }

    private void setupGridBag(GridBagConstraints gbc) {
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(5, 5, 5, 5);

        addComponents(gbc);

        btnValider.addActionListener(this::validerAction);
        btnEffacer.addActionListener(e -> effacerChamps());
        btnTerminer.addActionListener(e -> terminerAction());
    }

    private void addComponents(GridBagConstraints gbc) {
        String[] labels = {"Abscisse", "Ordonnée", "Valeur"};
        JTextField[] fields = {txtAbscisse, txtOrdonnee, txtValeur};
        JButton[] buttons = {btnValider, btnEffacer, btnTerminer};

        for (int i = 0; i < labels.length; i++) {
            gbc.gridx = i;
            gbc.gridy = 0;
            add(new JLabel(labels[i]), gbc);
            gbc.gridy = 1;
            add(fields[i], gbc);
            gbc.gridy = 2;
            add(buttons[i], gbc);
        }
    }

    private void validerAction(ActionEvent e) {
        boolean isValid = true;
        JTextField[] fields = {txtAbscisse, txtOrdonnee, txtValeur};
        for (JTextField field : fields) {
            try {
                if (field == txtAbscisse || field == txtOrdonnee) {
                    Integer.parseInt(field.getText());
                } else {
                    Double.parseDouble(field.getText());
                }
                field.setBackground(Color.WHITE);
            } catch (NumberFormatException ex) {
                field.setBackground(Color.RED);
                isValid = false;
            }
        }

        if (!isValid) {
            JOptionPane.showMessageDialog(this, "Veuillez entrer des valeurs numériques valides.", "Erreur de saisie", JOptionPane.ERROR_MESSAGE);
        } else {
            saveData();
        }
    }

    private void saveData() {
        try (FileWriter writer = new FileWriter("donnees.txt", true)) {
            writer.write(txtAbscisse.getText() + " " + txtOrdonnee.getText() + " " + txtValeur.getText() + "\n");
            JOptionPane.showMessageDialog(this, "Données enregistrées avec succès!");
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(this, "Erreur d'enregistrement : " + ex.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void terminerAction() {
        JOptionPane.showMessageDialog(this, "Merci de votre visite");
        System.exit(0);
    }

    private void effacerChamps() {
        txtAbscisse.setText("");
        txtOrdonnee.setText("");
        txtValeur.setText("");
        txtAbscisse.setBackground(Color.WHITE);
        txtOrdonnee.setBackground(Color.WHITE);
    }

    public static void main(String[] args) {
        new SaisiesSwing();
    }
}
