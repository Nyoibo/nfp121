/** Définir une position. */
public class Position {
	private int x;
	private int y;

	public Position(int x, int y) {
		this.x = x;
		this.y = y;
	}

	// Le Getter pour l'attribut x
	public int getX() {
		return x;
	}

	// Le Getter pour l'attribut y
	public int getY() {
		return y;
	}

	@Override public String toString() {
		return super.toString() + "(" + x + "," + y + ")";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Position position = (Position) o;
		return x == position.x && y == position.y;
	}
}
