import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import java.util.ArrayList;
import java.util.List;
import java.util.AbstractMap;
import java.util.AbstractMap.SimpleImmutableEntry;


/**
  * Donnees enregistre toutes les donnÃ©es reÃ§ues, quelque soit le lot.
  *
  * @author	Xavier CrÃ©gut <Prenom.Nom@enseeiht.fr>
  */
/** Définir une position. */
public class Donnees extends Traitement {
    private List<AbstractMap.SimpleImmutableEntry<Position, Double>> donnees;

    public Donnees() {
        this.donnees = new ArrayList<>();
    }

    @Override
    protected void gererDebutLotLocal(String nomLot) {
        // Logique spécifique pour le début d'un lot si nécessaire
    }

    @Override
    public void traiter(Position position, double valeur) {
        donnees.add(new SimpleImmutableEntry<>(position, valeur));
        for (Traitement suivant : this.suivants) {
            suivant.traiter(position, valeur);
        }
    }

    @Override
    protected void gererFinLotLocal(String nomLot) {
        // Logique spécifique pour la fin d'un lot si nécessaire
        afficherDonnees(nomLot);
    }

    private void afficherDonnees(String lot) {
        System.out.println("Données " + lot + " :");
        for (SimpleImmutableEntry<Position, Double> donnee : donnees) {
            Position pos = donnee.getKey();
            double val = donnee.getValue();
            System.out.println("- Position@" + pos.hashCode() + "(" +
                    pos.getX() + "," +
                    pos.getY() + ") -> " +
                    val);
        }
    }
}


