import java.util.*;
import java.util.HashMap;
import java.util.Map;

/**
  * SommeParPosition 
  *
  * @author	Xavier CrÃ©gut <Prenom.Nom@enseeiht.fr>
  */

public class SommeParPosition extends Traitement {

	// TODO Ã  faire...
    private Map<Position, Double> sommeParPosition;

    public SommeParPosition() {
        this.sommeParPosition = new HashMap<>();
    }

    @Override
    public void traiter(Position position, double valeur) {
        // Mettre à jour la somme pour la position donnée
        sommeParPosition.merge(position, valeur, Double::sum);

        // Passer la position et la valeur originale aux traitements suivants
        for (Traitement suivant : this.suivants) {
            suivant.traiter(position, valeur);
        }
    }

    @Override
    protected void gererFinLotLocal(String nomLot) {
        // Afficher les sommes par position à la fin du lot
        System.out.println("SommeParPosition " + nomLot + " :");
        sommeParPosition.forEach((position, somme) ->
                System.out.println("- " + position + " -> " + somme)
        );
        System.out.println("Fin SommeParPosition.");

        // Réinitialiser la map pour le prochain lot
        sommeParPosition.clear();
    }
}
