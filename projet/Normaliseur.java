import java.util.List;
import java.util.ArrayList;
/**
  * Normaliseur normalise les donnÃ©es d'un lot en utilisant une transformation affine.
  *
  * @author	Xavier CrÃ©gut <Prenom.Nom@enseeiht.fr>
  */
public class Normaliseur extends Traitement {

    // TODO Ã  faire...
    private double debut;
    private double fin;
    private double min = Double.MAX_VALUE;
    private double max = Double.MIN_VALUE;
    private List<Double> valeurs = new ArrayList<>();

    public Normaliseur(double debut, double fin) {
        this.debut = debut;
        this.fin = fin;
    }

    @Override
    public void traiter(Position position, double valeur) {
        // Mémorise la valeur pour la normalisation à la fin du lot
        valeurs.add(valeur);
        // Mettre à jour min et max
        if (valeur > max) {
            max = valeur;
        }
        if (valeur < min) {
            min = valeur;
        }
    }

    @Override
    protected void gererFinLotLocal(String nomLot) {
        // Calcule les paramètres de la transformation affine
        double a = (fin - debut) / (max - min);
        double b = debut - a * min;

        // Appliquer la transformation à toutes les valeurs mémorisées
        System.out.println(nomLot + " : Normalisation des valeurs");
        for (double valeur : valeurs) {
            double valeurNormalisee = a * valeur + b;
            System.out.println("- Position inconnue -> Valeur originale: " + valeur + ", Normalisée: " + valeurNormalisee);
        }

        // Réinitialisation pour le prochain lot
        valeurs.clear();
        min = Double.MAX_VALUE;
        max = Double.MIN_VALUE;
    }
}