/**
 * GenerateurXML Ã©crit dans un fichier, Ã  charque fin de lot, toutes
 * les donnÃ©es lues en indiquant le lot dans le fichier XML.
 *
 * @author	Xavier CrÃ©gut <Prenom.Nom@enseeiht.fr>
 */

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class GenerateurXML extends Traitement {

	// TODO Ã  faire...
    private String nomFichier;
    private List<Data> donnees = new ArrayList<>();

    public GenerateurXML(String nomFichier) {
        this.nomFichier = nomFichier;
    }

    @Override
    public void traiter(Position position, double valeur) {
        donnees.add(new Data(position, valeur));
        for (Traitement suivant : this.suivants) {
            suivant.traiter(position, valeur);
        }
    }

    @Override
    protected void gererFinLotLocal(String nomLot) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.newDocument();

            // Créer l'élément racine
            Element rootElement = doc.createElement("DonneesLot");
            doc.appendChild(rootElement);

            // Ajouter les données
            for (Data data : donnees) {
                Element dataElement = doc.createElement("Donnee");
                dataElement.setAttribute("lot", nomLot);
                dataElement.setAttribute("position", data.position.toString());
                dataElement.setAttribute("valeur", Double.toString(data.valeur));
                rootElement.appendChild(dataElement);
            }

            // Écrire le contenu dans un fichier XML
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(nomFichier));

            transformer.transform(source, result);
            System.out.println("Document XML créé : " + nomFichier);

        } catch (ParserConfigurationException | TransformerException e) {
            e.printStackTrace();
        }

        // Réinitialiser pour le prochain lot
        donnees.clear();
    }

    private static class Data {
        Position position;
        double valeur;

        Data(Position position, double valeur) {
            this.position = position;
            this.valeur = valeur;
        }
    }
}
