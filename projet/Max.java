/**
  * Max calcule le max des valeurs vues, quelque soit le lot.
  *
  * @author	Xavier CrÃ©gut <Prenom.Nom@enseeiht.fr>
  */

public class Max extends Traitement {

	// TODO Ã  faire...
    private double max = Double.NEGATIVE_INFINITY; // Initialise à la plus petite valeur pour un double

    @Override
    public void traiter(Position position, double valeur) {
        if (valeur > max) {
            max = valeur; // Mettre à jour max si la nouvelle valeur est plus grande
        }
        for (Traitement suivant : this.suivants) {
            suivant.traiter(position, valeur); // Continue la chaîne de traitement
        }
    }

    @Override
    protected void gererFinLotLocal(String nomLot) {
        // Afficher le maximum à la fin du lot
        System.out.println(nomLot + ": max = " + max);
        max = Double.NEGATIVE_INFINITY; // Réinitialiser pour le prochain lot
    }
}
