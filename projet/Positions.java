import java.util.ArrayList;
import java.util.List;

/**
  * Positions enregistre toutes les positions, quelque soit le lot.
  *
  * @author	Xavier CrÃ©gut <Prenom.Nom@enseeiht.fr>
  */
public class Positions extends PositionsAbstrait {

	// TODO Ã  faire...
    private List<Position> positions;

    public Positions(){
        this.positions = new ArrayList<>();
    }

    @Override
    public void traiter(Position position, double valeur) {
        super.traiter(position, valeur);
        positions.add(position);
    }

    @Override
    public int nombre() {
        return 0;
    }

    @Override
    public Position position(int indice) {
        return positions.get(indice);
    }

    @Override
    public int frequence(Position position) {
        int nombre = 0;

        for (Position p : this.positions) {

            if (p.equals(position)) {
                nombre += 1;
            }
        }
        return nombre;
    }
}
