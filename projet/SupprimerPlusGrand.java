/**
  * SupprimerPlusGrand supprime les valeurs plus grandes qu'un seuil.
  *
  * @author	Xavier CrÃ©gut <Prenom.Nom@enseeiht.fr>
  */
public class SupprimerPlusGrand extends Traitement {

	// TODO Ã  faire...
    private double seuil;

    public SupprimerPlusGrand(double seuil) {
        this.seuil = seuil;
    }

    @Override
    public void traiter(Position position, double valeur) {
        // Transmet la valeur seulement si elle est inférieure ou égale au seuil
        if (valeur <= seuil) {

                super.traiter(position, valeur);

        }
    }
}
