import java.lang.reflect.*;
import java.util.*;

/**
  * TraitementBuilder
  *
  * @author	Xavier CrÃ©gut <Prenom.Nom@enseeiht.fr>
  */
public class TraitementBuilder {

	/** Retourne un objet de type Class correspondant au nom en paramÃ¨tre.
	 * Exemples :
	 *   - int donne int.class
	 *   - Normaliseur donne Normaliseur.class
	 */
	Class<?> analyserType(String nomType) throws ClassNotFoundException {
			// TODO Ã  remplacer
        return switch (nomType) {
            case "int" -> int.class;
            case "double" -> double.class;
            case "String" -> String.class;
            default -> Class.forName(nomType);
        };
	}

	/** CrÃ©e l'objet java qui correspond au type formel en exploitant le Â« mot Â» suivant du scanner.
	 * Exemple : si formel est int.class, le mot suivant doit Ãªtre un entier et le rÃ©sulat est l'entier correspondant.
	 * Ici, on peut se limiter aux types utlisÃ©s dans le projet : int, double et String.
	 */
	static Object decoderEffectif(Class<?> formel, Scanner in) {
		in.useLocale(Locale.US);
		// Retourner la valeur en fonction du type attendu.
		if (formel == int.class) {
			return in.nextInt();
		} else if (formel == double.class) {
			return in.nextDouble();  // Retourne null si la prochaine entrée n'est pas un double
		} else if (formel == String.class) {
			return in.next();  // Retourne null si aucune donnée n'est disponible
		} else {
			throw new IllegalArgumentException("Type non pris en charge : " + formel);
		}
	}





	/** DÃ©finition de la signature, les paramÃ¨tres formels, mais aussi les paramÃ¨tres formels.  */
	static class Signature {
		Class<?>[] formels;
		Object[] effectifs;

		public Signature(Class<?>[] formels, Object[] effectifs) {
			this.formels = formels;
			this.effectifs = effectifs;
		}
	}

	/** Analyser une signature pour retrouver les paramÃ¨tres formels et les paramÃ¨tres effectifs.
	 * Exemple Â« 3 double 0.0 java.lang.String xyz int -5 Â» donne
	 *   - [double.class, String.class, int.class] pour les paramÃ¨tres formel et
	 *   - [0.0, "xyz", -5] pour les paramÃ¨tres effectifs.
	 */
	Signature analyserSignature(Scanner in) throws ClassNotFoundException {
		// TODO Ã  remplacer
		List<Class<?>> formel = new ArrayList<>();
		List<Object> effectifs = new ArrayList<>();
		String[] valeursIncorrect = {"int", "double", "java.lang.String"};


		while (in.hasNext()){
			String valeur = in.next();
			boolean isUpperCase = Character.isUpperCase(valeur.charAt(0));
			if (!isUpperCase && Arrays.stream(valeursIncorrect).anyMatch(valeur::equalsIgnoreCase)){
				Class<?> classeAttendu = analyserType(valeur);
				if(in.hasNext()){
					formel.add(classeAttendu);
					effectifs.add(decoderEffectif(classeAttendu, in));
				}else {
					throw new RuntimeException("Erreur le type formel" + valeur + "n'a pas de paramètre effectifs");
				}
			}

		}
		return new Signature(formel.toArray(new Class<?>[0]), effectifs.toArray());


	}




	/** Analyser la crÃ©ation d'un objet.
	 * Exemple : Â« Normaliseur 2 double 0.0 double 100.0 Â» consiste Ã  charger
	 * la classe Normaliseur, trouver le constructeur qui prend 2 double, et
	 * l'appeler en lui fournissant 0.0 et 100.0 comme paramÃ¨tres effectifs.
	 */
	Object analyserCreation(Scanner in)
		throws ClassNotFoundException, InvocationTargetException,
						  IllegalAccessException, NoSuchMethodException,
						  InstantiationException
	{
		// TODO Ã  remplacer
		String className = in.next();
		Class<?> cls = analyserType(className);
		Signature signature = analyserSignature(in);
		Constructor<?> constructor = cls.getConstructor(signature.formels);
		return constructor.newInstance(signature.effectifs);
	}


	/** Analyser un traitement.
	 * Exemples :
	 *   - Â« Somme 0 0 Â»
	 *   - Â« SupprimerPlusGrand 1 double 99.99 0 Â»
	 *   - Â« Somme 0 1 Max 0 0 Â»
	 *   - Â« Somme 0 2 Max 0 0 SupprimerPlusGrand 1 double 20.0 0 Â»
	 *   - Â« Somme 0 2 Max 0 0 SupprimerPlusGrand 1 double 20.0 1 Positions 0 0 Â»
	 * @param in le scanner Ã  utiliser
	 * @param env l'environnement oÃ¹ enregistrer les nouveaux traitements
	 */
	Traitement analyserTraitement(Scanner in, Map<String, Traitement> env)
			throws ClassNotFoundException, InvocationTargetException,
			IllegalAccessException, NoSuchMethodException,
			InstantiationException {

		env = env == null ? new HashMap<>() : env;
		List<Traitement> traitementList = new ArrayList<>();

		while (in.hasNext()){
			String className = in.next();
			boolean isUpperCase = Character.isUpperCase(className.charAt(0));
			if (isUpperCase){
				int param = in.nextInt();
				//
				StringBuilder creation = new StringBuilder(className + " " + param);
				if (param > 0) {
					for (int i = 0; i < param * 2 ; i++) {
						if (in.hasNext())
							creation.append(" ").append(in.next());
					}
				}
				Traitement traitement = (Traitement) analyserCreation(new Scanner(creation.toString()));
				traitementList.add(traitement);
				//
				env.put(className, traitement);
			}

		}
		for (int i =0; i < traitementList.size() - 1; i++){
			traitementList.get(0).ajouterSuivants(traitementList.get(i + 1));
		}
		//
		return traitementList.isEmpty() ? null : traitementList.get(0);

	}


	/** Analyser un traitement.
	 * @param in le scanner Ã  utiliser
	 * @param env l'environnement oÃ¹ enregistrer les nouveaux traitements
	 */
	public Traitement traitement(Scanner in, Map<String, Traitement> env)
	{
		try {
			return analyserTraitement(in, env);
		} catch (Exception e) {
			throw new RuntimeException("Erreur sur l'analyse du traitement, "
					+ "voir la cause ci-dessous", e);
		}
	}

}
