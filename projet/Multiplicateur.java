/**
  * Multiplicateur transmet la valeur multipliÃ©e par un facteur.
  *
  * @author	Xavier CrÃ©gut <Prenom.Nom@enseeiht.fr>
  */
public class Multiplicateur extends Traitement {

    private double facteur;

    /**
     * Constructeur de Multiplicateur qui initialise le facteur de multiplication.
     */
    public Multiplicateur(double facteur) {
        this.facteur = facteur;
    }

    @Override
    public void traiter(Position position, double valeur) {
        double nouvelleValeur = valeur * facteur;
        for (Traitement suivant : this.suivants) {
            suivant.traiter(position, nouvelleValeur);
        }
    }

    @Override
    protected void gererDebutLotLocal(String nomLot) {
        // C'est la Logique optionnelle pour le début d'un lot
    }

    @Override
    protected void gererFinLotLocal(String nomLot) {
        // C'est la Logique optionnelle pour la fin d'un lot
    }

    @Override
    protected String toStringComplement() {
        return "facteur=" + facteur;
    }

}

