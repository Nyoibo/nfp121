

/**
  * Somme calcule la sommee des valeurs, quelque soit le lot.
  *
  * @author	Xavier CrÃ©gut <Prenom.Nom@enseeiht.fr>
  */

public class Somme extends SommeAbstrait {

	// TODO Ã  faire...


	private double somme = 0;

	@Override
	public void traiter(Position position, double valeur) {
		super.traiter(position, valeur);
		somme+=valeur;

	}

	@Override
	public double somme() {
		return somme;
	}

	@Override
	public void gererFinLotLocal(String nomLot) {
		System.out.println(nomLot + ": somme = " + this.somme());
	}



}
