import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        // Assurez-vous que l'interface utilisateur est créée et mise à jour à partir du thread de distribution des événements (EDT).
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createAndShowGUI();
            }
        });
    }

    private static void createAndShowGUI() {
        // Créer l'instance de la fenêtre principale de votre application.
        SaisiesSwing saisiesSwing = new SaisiesSwing();

        // Définir la fenêtre pour qu'elle soit visible.
        saisiesSwing.setVisible(true);  // Appeler setVisible sur l'instance, pas la classe
    }
}

