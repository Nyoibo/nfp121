/**
  * SupprimerPlusPetit supprime les valeurs plus petites qu'un seuil.
  *
  * @author	Xavier CrÃ©gut <Prenom.Nom@enseeiht.fr>
  */
public class SupprimerPlusPetit extends Traitement {

	// TODO Ã  faire...
    private double seuil;

    public SupprimerPlusPetit(double seuil) {
        this.seuil = seuil;
    }

    @Override
    public void traiter(Position position, double valeur) {
        // Transmettre la valeur seulement si elle est supérieure au seuil
        if (valeur >= seuil) {

                super.traiter(position, valeur);

        }
    }
}
