import java.awt.*;

public class Cercle implements Mesurable2D{


    //Ajout de l'attribut centre du cercle
    private Point centre;

    //Ajout de l'attribut rayon du cercle
    private double rayon;

    //Ajout de l'attribut diametre du cercle
    private double diametre;

    //Ajout de l'attribut couleur du cercle
    private Color couleur;

    //E11 : ...
    // constructeur création cercle  à partir d’un point qui désigne son centre et d’un réel correspondant à la valeur de son rayon
    public Cercle(Point centre, double rayon) {
        this.centre = centre;
        this.rayon = rayon;
        this.diametre = rayon*2;
        this.couleur = Color.blue;
    }

    //E12 : On peut construire un cercle à partir de deux points diamétralement opposés...
    public Cercle(Point centre, Point rayon){
        this.centre = centre;
        this.rayon = centre.distance(rayon);
        this.couleur = Color.blue;
    }

    //E13 : On peut construire un cercle à partir de deux points diamétralement opposés et de sa couleur
    public Cercle(Point centre, Point rayon, Color couleur){
        this.centre = centre;
        this.rayon = centre.distance(rayon);
        this.couleur = couleur;

    }
    // public = accesseur
    // static =*
    //E14 : Une méthode de classe creerCercle(Point, Point) permet de créer un cercle à partir de deux points, le premier correspond au centre du cercle et le deuxième est un point du cercle
    public static Cercle creerCercle(Point centre, Point rayon){

        Cercle creerCercle = new Cercle(centre, rayon);
        return creerCercle;
    }


    //E15 : Méthode toString qui permet de d'afficher le cercle sur le terminal sous la forme suivant Cr@(a, b)
    public String toString(){
        return "C"+getRayon()+"@"+getCentre();
    }

    //E2 : On peut obtenir le centre d’un cercle
    public Point getCentre(){
        return this.centre;
    }

    //E4 : On peut obtenir le diamètre d’un cercle.
    public double getDiametre(){
        return this.diametre;
    }

    //E3 : On peut obtenir le rayon d’un cercle.
    public double getRayon(){
        return this.rayon;
    }

    //E9 : On peut obtenir la couleur d’un cercle.
    public Color getCouleur(){
        return this.couleur;
    }

    //E10 : On peut changer la couleur d’un cercle
    // Setter permet de changer la couleur
    public void setCouleur(Color newColor){
        this.couleur = newColor;

    }

    //E16 : On peut changer le rayon du cercle
    public void setRayon(double newRayon){
        this.rayon = newRayon;
        this.diametre = newRayon*2;
    }

    //E17 : On peut changer le diamètre du cercle.
    public void setDiametre(double newDiametre){
        this.diametre = newDiametre;
        this.rayon = newDiametre/2;
    }


    //E5 : On peut savoir si un point est à l’intérieur (au sens large) d’un cercle
    public boolean contient(Point x){
        if (this.getCentre().distance(x)<= this.getRayon()){
            return true;
        }
        else{
            return false;
        }

    }


    // E6 : Un cercle est un Mesurable2D (interface Mesurable2D). À ce titre, on peut obtenir son périmètre et son aire...
    //Calcule le périmètre du cercle
    @Override
    public double perimetre() {
        return 2*Math.PI*getRayon();
    }
    //Calcule de l'aire du cercle
    @Override
    public double aire() {
        return Math.PI * Math.pow(getRayon(),2);
    }


    //E1: On peut translater un cercle en précisant un déplacement suivant l’axe des X et un déplacement suivant l’axe des Y
    public void translater(double dx, double dy){
        this.getCentre().translater(dx, dy);
    }

















}